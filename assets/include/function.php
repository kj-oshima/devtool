<?php
/*==============================================
リンク付き画像を表示
================================================*/
function getimg($filename){

	$id = str_replace(".png", "", $filename);
	//$id = str_replace("_", "", $id);

	echo '<div class="c-img1" id="a'.$id.'">';
	echo '<a href="/assets/image/'.$filename.'" target="_blank"><img src="/assets/image/'.$filename.'"></a>';
	echo '</div>';
}

/*==============================================
ディレクトリ削除
================================================*/
function remove_directory($dir) {
	if ($handle = opendir("$dir")) {
		while (false !== ($item = readdir($handle))) {
			if ($item != "." && $item != "..") {
				if (is_dir("$dir/$item")) {
					remove_directory("$dir/$item");
				} else {
					unlink("$dir/$item");
				}
			}
		}
		closedir($handle);
		rmdir($dir);
	}
}

?>