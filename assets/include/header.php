<!DOCTYPE html>
<html lang="ja" id="pagetop">
<head>

<?php /*========================================
meta
================================================*/ ?>
<meta charset="UTF-8">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<?php /*========================================
viewport
================================================*/ ?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

<?php /*========================================
title
================================================*/ ?>
<title><?php echo str_replace(".php", "", basename($_SERVER['PHP_SELF'])); ?></title>

<?php /*========================================
css
================================================*/ ?>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<link href="/assets/bootflat/css/bootstrap.min.css" rel="stylesheet">
<link href="/assets/css/style.css" rel="stylesheet">


<?php /*========================================
js
================================================*/ ?>
<script
  src="https://code.jquery.com/jquery-2.2.4.min.js"
  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  crossorigin="anonymous"></script>
<script src="/assets/js/functions.js"></script>

</head>
<body>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/function.php'); ?>