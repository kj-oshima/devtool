<?php $id="top"; ?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>

<?php /*========================================
初期設定
================================================*/ ?>
<?php
session_cache_limiter('public');
session_start();

$project_name="";
$design="";
$git="";
$host="";
$localpath="";
$info="";
$sheet_outline="";
$sheet_sitemap="";

if(isset($_POST['project_name'])) {$project_name = $_POST['project_name'];}
if(isset($_POST['design'])) {$design = $_POST['design'];}
if(isset($_POST['git'])) {$git = $_POST['git'];}
if(isset($_POST['host'])) {$host = $_POST['host'];}
if(isset($_POST['localpath'])) {$localpath = $_POST['localpath'];}
if(isset($_POST['info'])) {$info = $_POST['info'];}
if(isset($_POST['sheet_outline'])) {$sheet_outline = $_POST['sheet_outline'];}
if(isset($_POST['sheet_sitemap'])) {$sheet_sitemap = $_POST['sheet_sitemap'];}
?>

<form action="index.php" method="post">

<div class="l-flame2">
<div class="l-flame2__left">

<?php /*========================================
プロジェクト名
================================================*/ ?>
<div class="panel panel-default"><div class="panel-heading">

	<h3 class="panel-title">プロジェクト名</h3>

</div><div class="panel-body">

	<div class="l-flame3">
		<div class="c-title1">プロジェクト名を入力。英語で好きな名前</div>
		<input type="text" name="project_name" value="<?php echo $project_name; ?>" class="form-control">
	</div>

</div></div>


<?php /*========================================
デザインデータ
================================================*/ ?>
<div class="panel panel-default"><div class="panel-heading">

	<h3 class="panel-title">デザインデータをアップロード</h3>

</div><div class="panel-body">


	<div class="l-flame3">
		<div class="c-title1 c-title1--gray">Googleドライブ フォルダ名 コピペ用</div>
		<input type="text" name="" value="Project <?php echo $project_name; ?>" class="form-control">
	</div>

	<div class="l-flame3">
		<a href="https://drive.google.com/drive/" target="_blank" class="btn btn-primary btn-block">自分のGoogleドライブにデザインデータを入れる</a>
	</div>

	<div class="l-flame3">
		<div class="c-title1">アップロードしたドライブURLを入力。共有設定も忘れずに！</div>
		<input type="text" name="design" value="<?php echo $design; ?>" class="form-control" placeholder="http://……">
	</div>

</div></div>

<?php /*========================================
概要
================================================*/ ?>
<div class="panel panel-default"><div class="panel-heading">

	<h3 class="panel-title">概要</h3>

</div><div class="panel-body">

	<div class="l-flame3">
		<div class="c-title1">レスポンシブとか、最優先で伝えた方がいいこと。</div>
		<textarea name="info" class="form-control"><?php echo $info; ?></textarea>
	</div>

	<br>
	【例】<br>
	* Only PC. Not responsive<br>
	* Only PC. Not responsive (However, there is no SP design yet.)<br>
	* Responsive<br>
	* WP

</div></div>

<?php /*========================================
Slackチャンネルの作成
================================================*/ ?>
<div class="panel panel-default"><div class="panel-heading">

	<h3 class="panel-title">Slackチャンネルの作成</h3>

</div><div class="panel-body">

	<div class="l-flame3">
		<div class="c-title1 c-title1--gray">この名前でSlackチャンネルを新規作成</div>
		<input type="text" name="" value="pr_<?php echo $project_name; ?>" class="form-control">
	</div>

</div></div>


<?php /*========================================
Slackチャンネルの作成
================================================*/ ?>
<div class="panel panel-default"><div class="panel-heading">

	<h3 class="panel-title">Gitリポジトリの追加</h3>

</div><div class="panel-body">

	<div class="l-flame3">
		<div class="c-title1 c-title1--gray">この名前でリポジトリを新規作成</div>
		<input type="text" name="" value="<?php echo $project_name; ?>" class="form-control">
	</div>

	<div class="l-flame3">
		<a href="https://reach.backlog.jp/settings/git/LABO_TASK/add" target="_blank" class="btn btn-primary btn-block">Gitリポジトリの追加</a>
	</div>


	<div class="l-flame3">
		<div class="c-title1">作成したリポジトリのURLを入力</div>
		<input type="text" name="git" value="<?php echo $git; ?>" class="form-control">
	</div>


</div></div>

<?php /*========================================
Slack通知設定
================================================*/ ?>
<div class="panel panel-default"><div class="panel-heading">

	<h3 class="panel-title">Slack通知設定</h3>

</div><div class="panel-body">

	<div class="l-flame3">
		<a href="http://flocssbem.test20008.com/page/121.php" target="_blank" class="btn btn-primary btn-block">この方法でBacklog Gitのpush通知をSlackに流す</a>
	</div>

</div></div>

<?php /*========================================
hosts設定
================================================*/ ?>
<div class="panel panel-default"><div class="panel-heading">

	<h3 class="panel-title">hosts設定</h3>

</div><div class="panel-body">

	<div class="l-flame3">
		<div class="c-title1 c-title1--gray">hostsの場所 コピペ用</div>
		<input type="text" name="" value="C:\Windows\System32\drivers\etc\hosts" class="form-control">
	</div>
	<div class="l-flame3">
		<div class="c-title1 c-title1--gray">これをhostsに追加</div>
		<input type="text" name="host2" value="127.0.0.1 <?php echo $project_name; ?>.cd" class="form-control">
	</div>

</div></div>

<?php /*========================================
localpath設定
================================================*/ ?>
<div class="panel panel-default"><div class="panel-heading">

	<h3 class="panel-title">localpath設定</h3>

</div><div class="panel-body">

	<div class="l-flame3">
		<div class="c-title1">コーディングファイルの置き場所を入力</div>
		<input type="text" name="localpath" value="<?php echo $localpath; ?>" placeholder="C:\xampp\htdocs\vhosts\chronodrive\aaa\bbb\www\httpdocs" class="form-control">
	</div>
	<div class="l-flame3">
		<div class="c-title1 c-title1--gray">httpd-vhostsの場所 コピペ用</div>
		<input type="text" name="" value="C:\xampp\apache\conf\extra\httpd-vhosts.conf" class="form-control">
	</div>
	<div class="l-flame3">
		<div class="c-title1 c-title1--gray">httpd-vhostsにこれを入力</div>
<textarea class="form-control" style="height:7em;">
<VirtualHost *:80>
DocumentRoot "<?php echo $localpath; ?>"
ServerName <?php echo $project_name; ?>.cd
</VirtualHost></textarea>
	</div>

</div></div>



<?php /*========================================
Clone
================================================*/ ?>
<div class="panel panel-default"><div class="panel-heading">

	<h3 class="panel-title">SorceTreeでリポジトリを初回Clone</h3>

</div><div class="panel-body">

	<div class="l-flame3">
		<div class="c-title1 c-title1--gray">リポジトリのURL コピペ用</div>
		<input type="text" name="" value="<?php echo $git; ?>" class="form-control">
	</div>
	<div class="l-flame3">
		<div class="c-title1 c-title1--gray">LocalPath コピペ用</div>
		<input type="text" name="" value="<?php echo $localpath; ?>" class="form-control">
	</div>

</div></div>



<?php /*========================================
xampp再起動
================================================*/ ?>
<div class="panel panel-default"><div class="panel-heading">

	<h3 class="panel-title">xampp再起動</h3>

</div><div class="panel-body">

	<div class="l-flame3">
		ここでxamppを再起動して、各設定を有効にします
	</div>

</div></div>



<?php /*========================================
作業ファイル入れる
================================================*/ ?>
<div class="panel panel-default"><div class="panel-heading">

	<h3 class="panel-title">コーディング用のテンプレートを入れる</h3>

</div><div class="panel-body">

	<div class="l-flame3">
		<div class="c-title1 c-title1--gray">コピー元</div>
		<input type="text" name="" value="C:\xampp\htdocs\vhosts\develop\template" class="form-control">
	</div>

	<div class="l-flame3">
		<div class="c-title1 c-title1--gray">コピー先</div>
		<input type="text" name="" value="<?php echo $localpath; ?>" class="form-control">
	</div>

	<div class="l-flame3">
		<a href="files.php" target="_blank" class="btn btn-primary btn-block">追加ファイル作成 files.php</a>
	</div>

	<div class="l-flame3">
		<div class="c-title1 c-title1--gray">追加ファイルの出来上がり場所</div>
		<input type="text" name="" value="C:\xampp\htdocs\vhosts\develop\devtool\files" class="form-control">
	</div>

	<div class="l-flame3">
		<div class="c-title1 c-title1--gray">gulpのホスト部分変更 コピペ用</div>
		<input type="text" name="" value="<?php echo $project_name; ?>.cd" class="form-control">
	</div>

</div></div>



<?php /*========================================
Push
================================================*/ ?>
<div class="panel panel-default"><div class="panel-heading">

	<h3 class="panel-title">リポジトリにpush</h3>

</div><div class="panel-body">

	<div class="l-flame3">
		初回pushしてください
	</div>

</div></div>


<?php /*========================================
シート作成
================================================*/ ?>
<div class="panel panel-default"><div class="panel-heading">

	<h3 class="panel-title">進行シート作成</h3>

</div><div class="panel-body">

	<div class="l-flame3">
		<a href="https://docs.google.com/spreadsheets/d/1xcGXERhQ36usZiilKPYLm2kCt8k3dsHn_tUcQp62mw8/edit#gid=0" target="_blank" class="btn btn-primary btn-block">このシート原本を自分のGoogleドライブにコピーして共有</a>
	</div>

	<div class="l-flame3">
		<p>右の欄のoutlineをシート内にコピペしたのち、URLを入力</p>
	</div>

	<div class="l-flame3">
		<div class="c-title1">シートのoutlineのURLを入力</div>
		<input type="text" name="sheet_outline" value="<?php echo $sheet_outline; ?>" placeholder="http://" class="form-control">
	</div>

	<div class="l-flame3">
		<p>sitemapを記載したのち、URLを入力</p>
	</div>
	<div class="l-flame3">
		<div class="c-title1">シートのsitemapのURLを入力</div>
		<input type="text" name="sheet_sitemap" value="<?php echo $sheet_sitemap; ?>" placeholder="http://" class="form-control">
	</div>

</div></div>




<?php /*========================================
Slack招待
================================================*/ ?>
<div class="panel panel-default"><div class="panel-heading">

	<h3 class="panel-title">Slackに招待</h3>

</div><div class="panel-body">

	<div class="l-flame3">
		・Slackチャンネルに作業者を招待してください。<br>
		・これを記載してください→
	</div>

</div></div>



</div>
<?php /*============================================================*/ ?>
<div class="l-flame2__right">

<?php /*========================================
更新
================================================*/ ?>
<div class="panel panel-default">
<div class="panel-body">
<input type="submit" name="send" value="更新" class="btn btn-warning btn-block">
</div>
</div>



<?php /*========================================
Outlineコピペ用
================================================*/ ?>
<div class="panel panel-default"><div class="panel-heading">

	<h3 class="panel-title">Outlineコピペ用</h3>

</div><div class="panel-body">

	<div class="l-flame3">
<textarea style="margin-top:0px; font-size:10px; height:30em;">
#Design data
<?php echo $design; ?>


#Git
<?php echo $git; ?>


#Local path
<?php echo $localpath; ?>


#Add to "hosts"
C:\Windows\System32\drivers\etc\hosts
127.0.0.1 <?php echo $project_name; ?>.cd

#Add to "httpd-vhosts.conf"
C:\xampp\apache\conf\extra\httpd-vhosts.conf

<VirtualHost *:80>
DocumentRoot "<?php echo $localpath; ?>"
ServerName <?php echo $project_name; ?>.cd
</VirtualHost>

#Info
<?php echo $info; ?></textarea>

</div>

</div></div>



<?php /*========================================
Slack記載 コピペ用
================================================*/ ?>
<div class="panel panel-default"><div class="panel-heading">

	<h3 class="panel-title">Slack記載 コピペ用</h3>

</div><div class="panel-body">

	<div class="l-flame3">
<textarea style="margin-top:0px; font-size:10px; height:10em;">
#Outline
<?php echo $sheet_outline; ?>


#Sitemap
<?php echo $sheet_sitemap; ?></textarea>

</div>

</form>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>

