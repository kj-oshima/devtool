<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>

<?php
/*==============================================
初期設定 スタイル
================================================*/?>
<style>
textarea.form-control{
	margin-top: 0;
	height: 12em;
}
select{
	margin-bottom: 20px;

}
</style>


<?php
/*==============================================
初期設定　PHP
================================================*/
session_cache_limiter('public');
session_start();

$files="";
if(isset($_POST['files'])) {
	$files = $_POST['files'];
}

$files = preg_replace("/^(\s)*(\r|\n|\r\n)/m", "", $files);

$array = explode("\n", $files);
$array = array_map('trim', $array);
$array = array_filter($array, 'strlen');
$array = array_values($array);


$type="";
if(isset($_POST['type'])) {
	$type = htmlspecialchars($_POST['type']);
}

if($array){

/*==============================================
ディレクトリ削除
================================================*/
$dir = "files";
remove_directory($dir);


for($i=0;$i<count($array);$i++){

/*==============================================
PHP 作成
================================================*/
$path ='files/';

if(!file_exists($path)){
	mkdir($path, 0777);
}

if($type=="php1"){
	$path = $path.'/'.$array[$i];
	$filename = $path.'/index.php';
	$html = file_get_contents('lib/files/page/index.txt', FILE_USE_INCLUDE_PATH);
	$html = str_replace("xxxxx", $array[$i], $html);
	if(!file_exists($path)){
		mkdir($path, 0777);
		file_put_contents($filename, $html);
	}

}elseif($type=="php2"){
	$path = $path;
	$filename = $path.$array[$i].'.php';
	$html = file_get_contents('lib/files/page/index.txt', FILE_USE_INCLUDE_PATH);
	$html = str_replace("xxxxx", $array[$i], $html);
	file_put_contents($filename, $html);

}elseif($type=="wp1"){
	$path = $path.'libs/';
	if(!file_exists($path)){
		mkdir($path, 0777);
	}
	$path = $path.'page/';
	if(!file_exists($path)){
		mkdir($path, 0777);
	}
	$filename = $path.$array[$i].'.php';
	$html = "";
	file_put_contents($filename, $html);
}


/*==============================================
画像　作成
================================================*/

$path ='files/assets';

if(!file_exists($path)){
	mkdir($path, 0777);
}

$path ='files/assets/image';

if(!file_exists($path)){
	mkdir($path, 0777);
}

$path =$path.'/'.$array[$i];

if(!file_exists($path)){
	mkdir($path, 0777);
	$filename = $path.'/.gitkeep';
	file_put_contents($filename, "");
}


/*==============================================
SCSS　作成
================================================*/
$path ='files/assets';

if(!file_exists($path)){
	mkdir($path, 0777);
}

$path ='files/assets/scss';

if(!file_exists($path)){
	mkdir($path, 0777);
}

$path ='files/assets/scss/3_project';

if(!file_exists($path)){
	mkdir($path, 0777);
}

if(file_exists($path)){
	$html = file_get_contents('lib/files/scss/index.txt', FILE_USE_INCLUDE_PATH);
	$html = str_replace("xxx", $array[$i], $html);
	$filename =$path.'/_'.$array[$i].'.scss';
	file_put_contents($filename, $html);
}

}

}


?>

<?php /*========================================
HTML
================================================*/ ?>
<form action="files.php" method="post">

<div class="l-frame1">

<?php /*========================================
条件
================================================*/ ?>
<div class="panel panel-default"><div class="panel-heading">

	<h3 class="panel-title">出力条件</h3>

</div><div class="panel-body">

<select name="type" class="form-control">
	<optgroup label="PHP">
		<option<?php if($type=="php1"){echo " selected";} ?> value="php1">ディレクトリあり</option>
		<option<?php if($type=="php2"){echo " selected";} ?> value="php2">ディレクトリなし</option>
	</optgroup>
	<optgroup label="Wordpress">
		<option<?php if($type=="wp1"){echo " selected";} ?> value="wp1">WP標準</option>
	</optgroup>
</select>


<div class="l-flame3">
<input type="submit" name="send" value="作成" class="btn btn-warning btn-block">
</div>

</div></div>



<?php /*========================================
入力
================================================*/ ?>
<div class="panel panel-default"><div class="panel-heading">

	<h3 class="panel-title">入力</h3>

</div><div class="panel-body">

	<div class="l-flame3">
		<div class="c-title1">必要なファイル名を入力</div>
<textarea name="files" class="form-control"><?php echo $files; ?></textarea>
	</div>

</div></div>



<?php /*========================================
SCSS
================================================*/ ?>
<div class="panel panel-default"><div class="panel-heading">

	<h3 class="panel-title">SCSS</h3>

</div><div class="panel-body">

	<div class="l-flame3">
		<div class="c-title1">style.scssに記載</div>
<textarea class="form-control">
<?php
for($i=0;$i<count($array);$i++){
	echo "@import '3_project/".$array[$i]."';";
	echo "
";
}
?>
</textarea>
</div>

</div></div>

<?php /*========================================
WP
================================================*/ ?>
<?php if($type=="wp1"){ ?>
<div class="panel panel-default"><div class="panel-heading">

	<h3 class="panel-title">WP</h3>

</div><div class="panel-body">

	<div class="l-flame3">
		<div class="c-title1">footer.phpに記載</div>
<textarea class="form-control">
<?php
for($i=0;$i<count($array);$i++){
	echo "add_page('".$array[$i]."','".$array[$i]."')";
	echo "
";
}
?>
</textarea>
</div>

</div></div>
<?php } ?>


<?php /*========================================
出力先
================================================*/ ?>
<div class="panel panel-default"><div class="panel-heading">

	<h3 class="panel-title">出力先</h3>

</div><div class="panel-body">

	<div class="l-flame3">
C:\xampp\htdocs\vhosts\develop\devtool\files\</div>

</div></div>




</div>
</div>
</form>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>